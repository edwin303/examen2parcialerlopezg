﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Examen2Parcialerlopezg.Interfaces;
using Examen2Parcialerlopezg.Models;
using Examen2Parcialerlopezg.Controllers;

namespace Examen2Parcialerlopezg.Views.UserControls
{
    /// <summary>
    /// Interaction logic for PersonForm.xaml
    /// </summary>
    public partial class PersonForm : UserControl, IForm<Agenda>
    {
        PersonController pc;
        public PersonForm()
        {
            InitializeComponent();
            SetupController();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Agenda GetData()
        {
            Agenda person = new Agenda
            {
                NombreLista = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                // BirthDate = (DateTime)BirthDatePicker.SelectedDate,
                //person.State = ((ComboBoxItem)StateCombobox.SelectedValue).Content.ToString();
                State = StateCombobox.SelectedValue.ToString()
            };
            return person;
        }

        public void SetData(Agenda data)
        {
            FirstNameTextBox.DataContext = data;
            LastNameTextBox.DataContext = data;
            //BirthDatePicker.DataContext = data;
            StateCombobox.DataContext = data;
        }

        protected void SetupController()
        {
            pc = new PersonController(this);
            this.SaveButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }
        public void Hide()
        {
            this.Visibility = Visibility.Collapsed;
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
