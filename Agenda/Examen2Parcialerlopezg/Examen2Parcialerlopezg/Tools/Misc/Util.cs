﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2Parcialerlopezg.Misc
{
    public static class Util
    {
        public static int GetAge(DateTime input)
        {
            int Age = DateTime.Now.Year - input.Year;
            int Monthbd = input.Month;
            int Daybd = input.Day;

            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;

            if (month < Monthbd)
            {
                Age--;
            }
            else
            {
                if (month == Monthbd)
                {
                    if (day < Daybd)
                    {
                        Age--;
                    }
                }
            }
            return Age;
        }
    }
}
