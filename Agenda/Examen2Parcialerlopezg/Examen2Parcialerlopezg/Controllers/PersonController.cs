﻿using Examen2Parcialerlopezg.Views.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using Examen2Parcialerlopezg.Models;
using Examen2Parcialerlopezg.Views.UserControls;
using System.Windows;
using Group = Examen2Parcialerlopezg.Models.Group;

namespace Examen2Parcialerlopezg.Controllers
{
    public class PersonController
    {
        object pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public PersonController(PersonWindow window)
        {
            pwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public PersonController(PersonForm form)
        {
            pwindow = form;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public void PersonEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Json File (*.json)|*.json";
            if (ofdialog.ShowDialog() == true)
            {
                Agenda p = new Agenda();
                if (pwindow.GetType().Equals(typeof(PersonWindow)))
                {
                    //((PersonWindow)pwindow).SetData(p.FromJson(ofdialog.FileName));
                }
                else
                {
                    ((PersonForm)pwindow).SetData(p.FromJson(ofdialog.FileName));
                }

            }
        }

        private void SaveData()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Agenda p;
                if (pwindow.GetType().Equals(typeof(PersonWindow)))
                {
                    p = ((PersonWindow)pwindow).GetData();
                }
                else
                {
                    p = ((PersonForm)pwindow).GetData();
                }

                p.ToJson(sfdialog.FileName);
            }
        }

        private void SaveCollection()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Group g = new Group
                {
                    Name = "Grupo de demostración",
                    Members = new List<Agenda>
                    {
                        new Agenda() { NombreLista = "William", LastName = "Sanchez", BirthDate = new System.DateTime(1983, 9, 1), State = "Item 1" }
                    }
                };

                Agenda p = new Agenda() { NombreLista = "Douglas", LastName = "Sanchez", BirthDate = new System.DateTime(1982, 08, 17), State = "Item 1" };
                g.Members.Add(p);

                //List<Person> people = new List<Person>()
                //{
                //    new Person() { FirstName = "Erick", LastName = "Sanchez", BirthDate = new System.DateTime(1984, 9, 1), State = "Masaya" },
                //    new Person() { FirstName = "Carolina", LastName = "Sanchez", BirthDate = new System.DateTime(1985, 6, 8),State="Masaya" },
                //    new Person() { FirstName = "Ricardo", LastName = "Sanchez", BirthDate = new System.DateTime(1988, 5, 6), State="Masaya" }
                //};
                g.Members.Add(new Agenda() { NombreLista = "Erick", LastName = "Sanchez", BirthDate = new System.DateTime(1984, 9, 1), State = "Item 1" });
                g.Members.Add(new Agenda() { NombreLista = "Carolina", LastName = "Sanchez", BirthDate = new System.DateTime(1985, 6, 8), State = "Item 1" });
                g.Members.Add(new Agenda() { NombreLista = "Ricardo", LastName = "Sanchez", BirthDate = new System.DateTime(1988, 5, 6), State = "Item 1" });

                g.ToJson(sfdialog.FileName);
            }
        }
    }
}
