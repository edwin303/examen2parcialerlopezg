﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using Examen2Parcialerlopezg.ViewsModels;
using Examen2Parcialerlopezg.Models;
using Examen2Parcialerlopezg.Views.Windows;
using Group = Examen2Parcialerlopezg.Models.Group;

namespace Examen2Parcialerlopezg.Controllers
{
    public class MainWindowController
    {
        private MainWindow mainWindow;
        public MainWindowController(MainWindow window)
        {
            mainWindow = window;
        }
        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;
                case "PersonMenuItem":
                    mainWindow.DataContext = new PersonViewModel();
                    break;
                case "PersonListMenuItem":
                    mainWindow.DataContext = new GroupViewModel() { Title = "Título de la ventana", Group = GetGroup() };
                    break;


            }
        }
        private Group GetGroup()
        {
            Group g = new Group
            {
                Name = "Los 4 Fantásticos",
                Members = new List<Agenda>
                    {
                        new Agenda() { NombreLista = "Peter", LastName = "Parker", BirthDate = new DateTime(2000, 1, 1), State = "Item 3" },
                        new Agenda() { NombreLista = "Susan", LastName = "Storm", BirthDate = new DateTime(1985, 1, 1), State = "Item 3" },
                        new Agenda() { NombreLista = "Johny", LastName = "Storm", BirthDate = new DateTime(1985, 1, 1),State="Item 3" },
                        new Agenda() { NombreLista = "Reed", LastName = "Richards", BirthDate = new DateTime(1980, 1, 1), State="Item 3" }
                    }
            };
            return g;
        }
    }
}
