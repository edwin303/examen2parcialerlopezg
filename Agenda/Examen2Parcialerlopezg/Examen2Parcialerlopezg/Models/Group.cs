﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen2Parcialerlopezg.Tools.Serialization;
using Examen2Parcialerlopezg.Interfaces;


namespace Examen2Parcialerlopezg.Models
{
    [Serializable]
    public class Group : IToFile, IFromFile<Group>
    {
        public string Name { get; set; }
        public int? Length { get { return Members?.Count; } }
        public ICollection<Agenda> Members { get; set; }

        public Group FromBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public Group FromJson(string filepath)
        {
            throw new NotImplementedException();
        }

        public Group FromXml(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            throw new NotImplementedException();
        }
    }
}
