﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen2Parcialerlopezg.Tools;
using Examen2Parcialerlopezg.Tools.Serialization;
using Examen2Parcialerlopezg.Interfaces;
//using Examen2Parcialerlopezg.Tools.Misc;

namespace Examen2Parcialerlopezg.Models
{
    [Serializable]
    public class Agenda : IToFile, IFromFile<Agenda>
    {
        public string NombreLista { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string State { get; set; }
        //public int Age { get { return Util.GetAge(BirthDate); } }

        public Agenda FromBinary(string filepath)
        {
            return BinarySerialization.ReadFromBinaryFile<Agenda>(filepath);
        }

        public Agenda FromJson(string filepath)
        {
            return JsonSerialization.ReadFromJsonFile<Agenda>(filepath);
        }

        public Agenda FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Agenda>(filepath);
        }

        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
