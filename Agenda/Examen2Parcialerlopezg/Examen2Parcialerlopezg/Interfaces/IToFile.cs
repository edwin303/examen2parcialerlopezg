﻿using System;
using System.Collections.Generic;

using System.Text;


namespace Examen2Parcialerlopezg.Interfaces
{
    public interface IToFile
    {
        void ToXml(string filepath);
        void ToJson(string filepath);
        void ToBinary(string filepath);

    }
}
