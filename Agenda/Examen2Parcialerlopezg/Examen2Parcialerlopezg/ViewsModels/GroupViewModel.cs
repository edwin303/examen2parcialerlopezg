﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen2Parcialerlopezg.Models;

namespace Examen2Parcialerlopezg.ViewsModels
{
    public class GroupViewModel
    {
        public string Title { get; set; }
        public Group Group { get; set; }
    }
}
